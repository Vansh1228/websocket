import express from "express";
import { createServer } from "http";
import { Server } from "socket.io";

const serverExpress = express();
const PORT = 3000;

serverExpress.get("/", (req, res) => {
  res.send("HELLO");
});

const serverSocket = createServer(serverExpress);
const io = new Server(serverSocket, {
  cors: {
    origin: "http://192.168.10.125:5173",
    methods: ["GET", "POST"],
    credentials: true,
  },
});

io.on("connection", (socket) => {
  console.log("USER CONNECTED");
  console.log("ID", socket.id);
  socket.emit("welcome", `Welcome to the server:${socket.id}`);

  socket.on("receive-message", (data) => {
    console.log(data);
    io.emit("receive-message", {
      id: socket.id,
      userName: data.userName,
      message: data.message,
    });
  });
});

serverSocket.listen(PORT, () => {
  console.log(`SERVER CREATED AND UP AND RUNNING ON PORT ${PORT}`);
});
