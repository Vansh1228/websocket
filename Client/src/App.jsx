import React, { useEffect, useState, useMemo } from "react";
import { io } from "socket.io-client";
function App() {
  const socket = useMemo(() => io("http://192.168.10.125:3000/"), []);

  const [message, setMessage] = useState("");
  const [storedMessages, setStoredMessages] = useState([]);
  const [userName, setUserName] = useState("");
  const handleSubmit = () => {
    socket.emit("receive-message", { userName, message });

    setMessage("");
  };

  useEffect(() => {
    socket.on("connect", () => {
      console.log("CONNECTED", socket.id);
    });

    socket.on("welcome", (event) => {
      console.log(event);
    });

    socket.on("receive-message", (messageData) => {
      setStoredMessages((prev) => [...prev, messageData]);
      console.log(messageData);
    });
    return () => {
      socket.off("receive-message");
    };
  }, [socket]);

  console.log(storedMessages);
  return (
    <div className="box">
      <input
        type="text"
        placeholder="Enter your username"
        value={userName}
        onChange={(e) => setUserName(e.target.value)}
      />
      <br />
      <input
        type="text"
        onChange={(e) => {
          setMessage(e.target.value);
        }}
      />

      <button onClick={handleSubmit}>Send</button>
      <ul>
        {storedMessages.map(({ userName, message }) => (
          <li>
            {userName}: {message}
          </li>
        ))}
      </ul>
      <div className="data"></div>
    </div>
  );
}

export default App;
