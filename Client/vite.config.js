import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
// import { address } from 'ip'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    host: '0.0.0.0'
  },
  '/socket.io': {
    target: 'ws://localhost:5173',
    ws: true,
  },
})
